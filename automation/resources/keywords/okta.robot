*** Settings ***
Library           SeleniumLibrary
Library           ImageHorizonLibrary
Library           AutoItLibrary

*** Keywords ***
Login Workspace Web With Okta Idp
    [Arguments]    ${url}    ${username}    ${password}    ${browser_cache}
    Set Reference Folder    ${CURDIR}${/}..${/}..${/}resources${/}images${/}okta${/}
    Set Confidence    0.5
    open browser    ${url}    chrome    options=add_argument("--start-maximized");add_argument("user-data-dir=${browser_cache}")
    wait until page contains    Sign In    timeout=120
    input text    id=okta-signin-username    ${username}
    input text    id=okta-signin-password    ${password}
    click element    id=okta-signin-submit
    ${status}    ${value} =    Run Keyword And Ignore Error    wait until page contains    Detect Workspace    timeout=20
    Run Keyword If    '${status}' == 'PASS'    click button    Detect Workspace
    sleep    5
    Run Keyword If    '${status}' == 'PASS'    ImageHorizonLibrary.Click Image    btn_open_citrix_workspace_launcher.png
    wait until page contains    Search Workspace    timeout=40
