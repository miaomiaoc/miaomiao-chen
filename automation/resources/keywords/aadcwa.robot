*** Settings ***
Library           SeleniumLibrary
Library           ImageHorizonLibrary
Library           AutoItLibrary

*** Keywords ***
Login Workspace cwa With AAd Idp
    [Arguments]    ${url}    ${username}    ${password}    ${browser_cache}
    Set Reference Folder    ${CURDIR}${/}..${/}..${/}resources${/}images${/}aad${/}
    Set Confidence    0.5
    Launch Application    "C:\\Program Files (x86)\\Citrix\\ICA Client\\SelfServicePlugin\\CleanUp.exe"    alias=CleanUp
    Wait For    cwa_cleanup.png    timeout=20
    ImageHorizonLibrary.Click To The Above Of Image    cwa_cleanup.png    0    clicks=1    interval=1
    sleep    5
    Wait For    cwa_cleanok.png    timeout=20
    ImageHorizonLibrary.Click To The Above Of Image    cwa_cleanok.png    0    clicks=1    interval=1
    Wait For    cwa_url.png    timeout=20
    ImageHorizonLibrary.Click To The Above Of Image    cwa_url.png    0    clicks=1    interval=1
    Type    ${url}
    Wait For    cwa_add.png    timeout=10
    ImageHorizonLibrary.Click To The Above Of Image    cwa_add.png    0    clicks=1    interval=1
    Wait For    cwa_username.png    timeout=60
    Type    ${username}
    Send    {ENTER}
    sleep    3
    Type    ${password}
    Send    {ENTER}
    sleep    3
    ${status}    ${value} =    Run Keyword And Ignore Error    Wait For    btn_yes1.png    timeout=10
    Run Keyword If    '${status}' == 'PASS'    ImageHorizonLibrary.Click To The Above Of Image    btn_yes1.png    0    clicks=1    interval=1
