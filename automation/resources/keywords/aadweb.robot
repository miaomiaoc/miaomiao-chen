*** Settings ***
Library           SeleniumLibrary
Library           ImageHorizonLibrary
Library           AutoItLibrary

*** Keywords ***
Login Workspace Web With AAd Idp
    [Arguments]    ${url}    ${username}    ${password}    ${browser_cache}
    Set Reference Folder    ${CURDIR}${/}..${/}..${/}resources${/}images${/}aad${/}
    Set Confidence    0.5
    open browser    ${url}    chrome    options=add_argument("--start-maximized");add_argument("user-data-dir=${browser_cache}")
    wait until page contains    Sign in    timeout=120
    sleep    1
    input text    id=i0116    ${username}
    click element    id=idSIButton9
    input text    id=i0118    ${password}
    Wait For    btn_log_on.png    timeout=10
    ImageHorizonLibrary.Click To The Above Of Image    btn_log_on.png    0    clicks=2    interval=1
    sleep    3
    ${status}    ${value} =    Run Keyword And Ignore Error    Wait For    btn_yes.png    timeout=20
    Run Keyword If    '${status}' == 'PASS'    ImageHorizonLibrary.Click To The Above Of Image    btn_yes.png    0    clicks=3    interval=1
    ${status}    ${value} =    Run Keyword And Ignore Error    wait until page contains    Use web browser    timeout=20
    Run Keyword If    '${status}' == 'PASS'    click button    Use web browser
    sleep    5
    wait until page contains    Search Workspace    timeout=40
