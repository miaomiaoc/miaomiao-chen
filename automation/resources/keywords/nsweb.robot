*** Settings ***
Library           SeleniumLibrary
Library           ImageHorizonLibrary
Library           AutoItLibrary

*** Keywords ***
Login Workspace Web With Ns Idp
    [Arguments]    ${url}    ${username}    ${password}    ${browser_cache}
    Set Reference Folder    ${CURDIR}${/}..${/}..${/}resources${/}images${/}ns${/}
    Set Confidence    0.5
    open browser    ${url}    chrome    options=add_argument("--start-maximized");add_argument("user-data-dir=${browser_cache}")
    wait until page contains    Log On    timeout=120
    input text    id=login    ${username}
    input text    id=passwd    ${password}
    click element    id=nsg-x1-logon-button
    ${status}    ${value} =    Run Keyword And Ignore Error    wait until page contains    Use web browser    timeout=20
    Run Keyword If    '${status}' == 'PASS'    click button    Use web browser
    sleep    2
    wait until page contains    Search Workspace    timeout=40
