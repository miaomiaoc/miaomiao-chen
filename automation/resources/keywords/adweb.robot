*** Settings ***
Library           SeleniumLibrary
Library           ImageHorizonLibrary
Library           AutoItLibrary

*** Keywords ***
Login Workspace Web With Ad Idp
    [Arguments]    ${url}    ${username}    ${password}    ${browser_cache}
    Set Reference Folder    ${CURDIR}${/}..${/}..${/}resources${/}images${/}ad${/}
    Set Confidence    0.5
    open browser    ${url}    chrome    options=add_argument("--start-maximized");add_argument("user-data-dir=${browser_cache}")
    wait until page contains    Log On    timeout=120
    input text    id=username    ${username}
    input text    id=password    ${password}
    click element    id=loginBtn
    ${status}    ${value} =    Run Keyword And Ignore Error    wait until page contains    Use web browser    timeout=20
    Run Keyword If    '${status}' == 'PASS'    click button    Use web browser
    sleep    5
    wait until page contains    Search Workspace    timeout=40
