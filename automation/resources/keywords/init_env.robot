*** Settings ***
Variables         ${CURDIR}${/}..${/}..${/}resources${/}configuration${/}settings.yaml

*** Keywords ***
Init environment
    Set Global Variable    ${ad_idp}    ${idp.ad}
    Set Global Variable    ${okta_idp}    ${idp.okta}
    Set Global Variable    ${ns_idp}    ${idp.ns}
    Set Global Variable    ${aad_idp}    ${idp.aad}
    Set Global Variable    ${browser_cache}    ${browser_cache}    