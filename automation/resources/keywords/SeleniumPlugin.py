from robot.api import logger
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from SeleniumLibrary.base import LibraryComponent, keyword


class SeleniumPlugin(LibraryComponent):

    def __init__(self, ctx):
        LibraryComponent.__init__(self, ctx)

    @keyword
    def minimize_browser(self):
        logger.info('Executing minimize window...')
        self.driver.minimize_window()
    
    
    

