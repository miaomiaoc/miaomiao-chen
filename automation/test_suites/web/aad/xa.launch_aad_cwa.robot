*** Settings ***
Suite Teardown
Library           AutoItLibrary
Resource          ../../../resources/keywords/aadcwa.robot

*** Test Cases ***
cwa launch Application ${aad_idp.url}
    [Tags]    smoke
    Login Workspace cwa With AAd Idp    ${aad_idp.url}    ${aad_idp.username}    ${aad_idp.password}    ${browser_cache}
    sleep    30
    Set Confidence    0.5
    Wait For    cwa_App.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_App.png    0    clicks=2    interval=1
    Wait For    icon_connecting3.png    timeout=60
    Wait For    staging_connectong3.png    timeout=50
    ImageHorizonLibrary.Click To The Above Of Image    staging_connectong3.png    0    clicks=1    interval=1
    sleep    5
    Type    ${aad_idp.username}
    Send    {TAB}
    Type    ${aad_idp.password}
    Send    {ENTER}
    sleep    15
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    20
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    5
