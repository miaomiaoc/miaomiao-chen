*** Settings ***
Test Teardown     Close All Browsers
Resource          ../../../resources/keywords/aad.robot

*** Test Cases ***
login web ${aad_idp.url}
    [Tags]    smoke
    Login Workspace Web With AAd Idp    ${aad_idp.url}    ${aad_idp.username}    ${aad_idp.password}    ${browser_cache}
