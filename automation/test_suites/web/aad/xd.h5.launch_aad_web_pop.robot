*** Settings ***
Test Teardown     Close All Browsers
Library           SeleniumLibrary    plugins=${CURDIR}/../../../resources/keywords/SeleniumPlugin.py
Resource          ../../../resources/keywords/aad.robot

*** Test Cases ***
web launch xaxd with h5 And verify connection (with pop) ${aad_idp.url}
    [Tags]    smoke
    Login Workspace Web With AAd Idp    ${aad_idp.url}    ${aad_idp.username}    ${aad_idp.password}    ${browser_cache}
    wait until page contains    stagingaadvda    timeout=30
    Click Element    class=wsui-qvfmvs
    Click Element    class=wsui-1j9u7tz
    Wait For    advanced.png    timeout=10
    ImageHorizonLibrary.Click To The Above Of Image    advanced.png    0    clicks=2    interval=1
    sleep    10
    ${status}    ${value} =    Run Keyword And Ignore Error    Wait For    h5_cwa.png    timeout=20
    sleep    5
    Run Keyword If    '${status}' == 'FAIL'    ImageHorizonLibrary.Click Image    h5_tocwa.png
    sleep    10
    Run Keyword If    '${status}' == 'FAIL'    ImageHorizonLibrary.Click Image    btn_open_citrix_workspace_launcher.png
    sleep    2
    Click Element    class=icon_qqq3q8
    wait until page contains    stagingaadvda    timeout=30
    Click Element    xpath://div[@title="stagingaadvda"]
    sleep    5
    ImageHorizonLibrary.Click Image    btn_open_citrix_workspace_launcher.png
    sleep    5
    Minimize Browser
    Wait For    icon_connecting.png    timeout=60
    Wait For    staging_connectong3.png    timeout=50
    ImageHorizonLibrary.Click To The Above Of Image    staging_connectong3.png    0    clicks=1    interval=1
    sleep    5
    Type    ${aad_idp.username}
    Send    {TAB}
    Type    ${aad_idp.password}
    Send    {ENTER}
    sleep    10
    Wait For    cwa_disconnect1.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect1.png    0    clicks=1    interval=1
    Wait For    cwa_disconnect2.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect2.png    0    clicks=2    interval=1
    Wait For    cwa_disconnect3.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect3.png    0    clicks=2    interval=1
    sleep    10
