*** Settings ***
Test Teardown     Close All Browsers
Library           SeleniumLibrary    plugins=${CURDIR}/../../../resources/keywords/SeleniumPlugin.py
Resource          ../../../resources/keywords/aad.robot

*** Test Cases ***
web launch xaxd with h5 And verify connection ${aad_idp.url}
    [Tags]    smoke
    Login Workspace Web With AAd Idp    ${aad_idp.url}    ${aad_idp.username}    ${aad_idp.password}    ${browser_cache}
    Click Element    class=wsui-qvfmvs
    Click Element    class=wsui-1j9u7tz
    Wait For    advanced1.png    timeout=10
    ImageHorizonLibrary.Click To The Above Of Image    advanced.png    0    clicks=2    interval=1
    sleep    10
    ${status}    ${value} =    Run Keyword And Ignore Error    Wait For    h5_cwa.png    timeout=20
    sleep    5
    Run Keyword If    '${status}' == 'FAIL'    ImageHorizonLibrary.Click Image    h5_tocwa.png
    sleep    10
    Run Keyword If    '${status}' == 'FAIL'    ImageHorizonLibrary.Click Image    btn_open_citrix_workspace_launcher.png
    sleep    6
