*** Settings ***
Suite Teardown
Resource          ../../../resources/keywords/aadcwa.robot

*** Test Cases ***
login cwa ${aad_idp.url}
    [Tags]    smoke
    Login Workspace cwa With AAd Idp    ${aad_idp.url}    ${aad_idp.username}    ${aad_idp.password}    ${browser_cache}
    sleep    30
    Wait For    cwa_App.png    timeout=40
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    10
