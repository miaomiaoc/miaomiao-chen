*** Settings ***
Suite Teardown
Resource          ../../../resources/keywords/nscwa.robot

*** Test Cases ***
cwa launch Desktop ${ns_idp.url}
    [Tags]    smoke
    Login Workspace cwa With Ns Idp    ${ns_idp.url}    ${ns_idp.username}    ${ns_idp.password}    ${browser_cache}
    sleep    30
    Set Confidence    0.5
    Wait For    cwa_desktop.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_desktop.png    0    clicks=2    interval=1
    Wait For    icon_connecting.png    timeout=60
    Wait For    staging_connectong.png    timeout=50
    ImageHorizonLibrary.Click To The Above Of Image    staging_connectong.png    0    clicks=1    interval=1
    sleep    5
    Type    ${ns_idp.username}
    Send    {TAB}
    Type    ${ns_idp.password}
    Send    {ENTER}
    sleep    10
    Wait For    cwa_disconnect1.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect1.png    0    clicks=2    interval=1
    Wait For    cwa_disconnect2.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect2.png    0    clicks=2    interval=1
    Wait For    cwa_disconnect3.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect3.png    0    clicks=2    interval=1
    sleep    10
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    10
