*** Settings ***
Suite Teardown
Resource          ../../../resources/keywords/nscwa.robot

*** Test Cases ***
login cwa ${ns_idp.url}
    [Tags]    smoke
    Login Workspace cwa With Ns Idp    ${ns_idp.url}    ${ns_idp.username}    ${ns_idp.password}    ${browser_cache}
    sleep    30
    Wait For    cwa_Notepad.png    timeout=40
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    10
