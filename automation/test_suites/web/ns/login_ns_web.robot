*** Settings ***
Suite Teardown
Test Teardown     Close All Browsers
Resource          ../../../resources/keywords/ns.robot

*** Test Cases ***
login web ${ns_idp.url}
    [Tags]    smoke
    Login Workspace Web With Ns Idp    ${ns_idp.url}    ${ns_idp.username}    ${ns_idp.password}    ${browser_cache}
