*** Settings ***
Suite Teardown
Resource          ../../../resources/keywords/adcwa.robot

*** Test Cases ***
cwa launch Application ${ad_idp.url}
    [Tags]    smoke
    Login Workspace cwa With Ad Idp    ${ad_idp.url}    ${ad_idp.username}    ${ad_idp.password}    ${browser_cache}
    sleep    30
    Set Confidence    0.5
    Wait For    cwa_App.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_App.png    0    clicks=2    interval=1
    Wait For    icon_connecting3.png    timeout=60
    Wait For    cwa_cal.png    timeout=100
    sleep    10
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    20
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    5
