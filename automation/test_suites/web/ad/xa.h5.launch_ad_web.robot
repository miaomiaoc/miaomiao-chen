*** Settings ***
Test Teardown     Close All Browsers
Library           SeleniumLibrary    plugins=${CURDIR}/../../../resources/keywords/SeleniumPlugin.py
Resource          ../../../resources/keywords/ad.robot

*** Test Cases ***
web launch xaxd with h5 And verify connection ${ad_idp.url}
    [Tags]    smoke
    Login Workspace Web With Ad Idp    ${ad_idp.url}    ${ad_idp.username}    ${ad_idp.password}    ${browser_cache}
    wait until page contains    Calculator    timeout=30
    Click Element    class=wsui-qvfmvs
    Click Element    class=wsui-1j9u7tz
    Wait For    advanced.png    timeout=10
    ImageHorizonLibrary.Click To The Above Of Image    advanced.png    0    clicks=2    interval=1
    sleep    10
    ${status}    ${value} =    Run Keyword And Ignore Error    Wait For    h5_cwa.png    timeout=20
    sleep    5
    Run Keyword If    '${status}' == 'FAIL'    ImageHorizonLibrary.Click Image    h5_tocwa.png
    sleep    10
    Run Keyword If    '${status}' == 'FAIL'    ImageHorizonLibrary.Click Image    btn_open_citrix_workspace_launcher.png
    sleep    6
    Click Element    class= wsui-1dmkhln
    wait For    h5.png    timeout=10
    Click Element    class=wsui-1j726ay
    sleep    2
    Click Element    class=icon_qqq3q8
    wait until page contains    Calculator    timeout=30
    Click Element    xpath://div[@title="Calculator"]
    Wait For    cwa_cal    timeout=100
    sleep    2
    SeleniumLibrary.Switch Window    NEW
    SeleniumLibrary.Close Window
    SeleniumLibrary.Switch Window    MAIN
    Click Element    class=wsui-qvfmvs
    Click Element    class=wsui-1j9u7tz
    Wait For    advanced.png    timeout=10
    ImageHorizonLibrary.Click To The Above Of Image    advanced.png    0    clicks=2    interval=1
    Click Element    class=wsui-1dmkhln
    sleep    2
    Wait For    btn_open_citrix_workspace_launcher.png    timeout=10
    ImageHorizonLibrary.Click To The Above Of Image    btn_open_citrix_workspace_launcher.png    0    clicks=2    interval=1
    sleep    5
