*** Settings ***
Suite Teardown
Resource          ../../../resources/keywords/adcwa.robot

*** Test Cases ***
login cwa ${ad_idp.url}
    [Tags]    smoke
    Login Workspace cwa With Ad Idp    ${ad_idp.url}    ${ad_idp.username}    ${ad_idp.password}    ${browser_cache}
    sleep    30
    Wait For    cwa_App.png    timeout=40
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    10
