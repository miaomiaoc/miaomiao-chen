*** Settings ***
Suite Teardown
Resource          ../../../resources/keywords/adcwa.robot

*** Test Cases ***
cwa launch Desktop ${ad_idp.url}
    [Tags]    smoke
    Login Workspace cwa With Ad Idp    ${ad_idp.url}    ${ad_idp.username}    ${ad_idp.password}    ${browser_cache}
    sleep    30
    Set Confidence    0.5
    Wait For    cwa_desktop.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_desktop.png    0    clicks=2    interval=1
    Wait For    icon_connecting2.png    timeout=30
    Wait For    staging_desktop.png    timeout=60
    sleep    10
    Wait For    cwa_disconnect1.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect1.png    0    clicks=2    interval=1
    Wait For    cwa_disconnect2.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect2.png    0    clicks=2    interval=1
    Wait For    cwa_disconnect3.png    timeout=40
    ImageHorizonLibrary.Click To The Above Of Image    cwa_disconnect3.png    0    clicks=2    interval=1
    sleep    10
    ImageHorizonLibrary.Click Image    btn_desktop_close.png
    sleep    10
