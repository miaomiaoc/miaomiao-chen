*** Settings ***
Test Teardown     Close All Browsers
Resource          ../../../resources/keywords/ad.robot

*** Test Cases ***
login web ${ad_idp.url}
    [Tags]    smoke
    Login Workspace Web With Ad Idp    ${ad_idp.url}    ${ad_idp.username}    ${ad_idp.password}    ${browser_cache}
